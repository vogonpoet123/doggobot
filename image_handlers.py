from logger import log_to_db


def get_dog_image(bot, update):
    log_to_db(bot, update)

    x = bot.db.get_collection('doggobot').aggregate(
        [{"$match": {"used": False}}, {"$sample": {"size": 1}}])
    x = list(x)

    if len(x) == 0:
        bot.db.get_collection('doggobot').update_many(
            {}, {"$set": {"used": False}})

        x = list(bot.db.get_collection('doggobot').aggregate(
            [{"$match": {"used": False}}, {"$sample": {"size": 1}}]))

    bot.db.get_collection('doggobot').update_one({"_id": x[0]['_id']}, {"$set": {"used": True}})

    image_type = x[0]['type']
    link = x[0]['link']

    if image_type in ['gif', 'gifv', 'mp4']:
        bot.send_document(chat_id=update.message.chat_id, document=link)
    else:
        bot.send_photo(chat_id=update.message.chat_id, photo=link)


def add_image_by_link(bot, update, args):
    if update.message.from_user.id in [302340229]:
        bot.db.get_collection('doggobot').insert_one(
            {'link': args[0], 'type': args[0].split('.')[-1], 'used': False})
    else:
        bot.send_message(chat_id=update.message.chat.id,
                         text="You are not authorized")


def add_image_by_user(bot, update):
    if update.message.reply_to_message is not None and (len(update.message.reply_to_message.photo) != 0 or update.message.reply_to_message.animation is not None):

        if update.message.reply_to_message.animation is not None:
            file_id = update.message.reply_to_message.animation.file_id
        else:
            file_id = update.message.reply_to_message.photo[-1].file_id
        print(file_id)

        # Adding a record to the DB
        bot.db.get_collection('doggobot').insert_one(
            {
                'link': file_id,
                'type': "file_id_tg_servers",
                'used': False,
                "added_by": update.message.from_user.id
            })

        # Confirmation the image was added
        bot.send_message(chat_id=update.message.chat.id, text="Saved")

    else:
        bot.send_message(chat_id=update.message.chat.id,
                         text="Type /add while replying to an image to add it to doggobot")
