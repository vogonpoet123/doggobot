# -*- coding: utf-8 -*-

from telegram.bot import Bot
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import os
from image_handlers import get_dog_image, add_image_by_link, add_image_by_user
from weather_handlers import get_weather
from misc_handlers import print_info, custom_command_handler, add_custom_handler, list_all_commands, alternating_caps
from error import error_handler
from authorize import mongo, psql
import sentry_sdk

# Constants
port = int(os.environ.get('PORT', '8443'))
debug = os.environ.get('DEBUG', "True")
sentry_sdn = os.environ.get('SENTRY_SDN', "sdn")


class DoggoBot(Bot):
    def __init__(self, *args, **kwargs):
        super(DoggoBot, self).__init__(*args, **kwargs)
        self.cursor, self.conn = psql()
        self.db = mongo()


def main():
    token = os.environ.get('token', None)
    bot_doggo = DoggoBot(token)
    updater = Updater(bot=bot_doggo)
    dispatcher = updater.dispatcher

    # Command Handlers
    dispatcher.add_handler(CommandHandler('dog', get_dog_image))
    dispatcher.add_handler(CommandHandler('addf', add_image_by_link, pass_args=True))
    dispatcher.add_handler(CommandHandler('wr', get_weather, pass_args=True))
    dispatcher.add_handler(CommandHandler('add', add_image_by_user))
    dispatcher.add_handler(CommandHandler('info', print_info))
    dispatcher.add_handler(CommandHandler('ac', add_custom_handler, pass_args=True))
    dispatcher.add_handler(CommandHandler('commands', list_all_commands))
    dispatcher.add_handler(CommandHandler('altcap', alternating_caps, pass_args=True))

    # Handles any other command that may come
    dispatcher.add_handler(MessageHandler(Filters.command, custom_command_handler))

    if debug == "True":
        print("Debug mode, polling!")
        updater.start_polling()

    else:
        sentry_sdk.init(sentry_sdn)
        updater.start_webhook(listen="0.0.0.0", port=port, url_path=token)
        updater.bot.set_webhook(
            "https://likesmolanimals.herokuapp.com/" + token)
        updater.idle()


if __name__ == "__main__":
    main()
