import psycopg2
import os
from pymongo import MongoClient


def psql():
    db_user = os.environ.get('db_user', 'vfbjbmem')
    db_name = os.environ.get('db_name', 'vfbjbmem')
    db_pass = os.environ.get('db_pass', '123')
    db_host = os.environ.get('db_host', "manny.db.elephantsql.com")

    conn = psycopg2.connect(database=db_name, user=db_user, password=db_pass, host=db_host)
    cursor = conn.cursor()
    return cursor, conn


def mongo():
    mongo_host = os.environ.get('mongo_host', None)
    mongo_db = os.environ.get('mongo_db', None)
    mongo_user = os.environ.get('mongo_user', None)
    mongo_pwd = os.environ.get('mongo_pwd', None)

    client = MongoClient(mongo_host.format(mongo_user, mongo_pwd))
    db = client[mongo_db]
    return db