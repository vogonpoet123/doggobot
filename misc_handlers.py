from telegram.error import TelegramError
import re
from logger import log_to_db, log_to_sentry
from sentry_sdk import capture_exception
from random import random

SEND_FILE = 'send_file'
GIF = 'animation'
IMAGE = 'photo'
VIDEO = 'video'
STICKER = 'sticker'
DOCUMENT = 'document'
AUDIO = 'audio'
VOICE_MESSAGE = 'voice'

MEDIA_TYPES = [GIF, IMAGE, VIDEO, STICKER, DOCUMENT]


def custom_command_handler(bot, update):
    log_to_db(bot, update)
    reply_message_id = None
    command_fired = update.message.text.split('/')[-1].split('@')[0]

    if update.message.reply_to_message is not None:
        reply_message_id = update.message.reply_to_message.message_id

    action = bot.db.get_collection('commands').find_one({"cmd": command_fired, "bot_id": bot.id})
    if action is not None:
        if action['action'] == SEND_FILE:
            file_type = action['file_type']
            try:
                if file_type == IMAGE:
                    bot.send_photo(update.message.chat_id, action['action_data'],
                                   reply_to_message_id=reply_message_id)
                elif file_type == VIDEO:
                    bot.send_video(update.message.chat_id, action['action_data'],
                                   reply_message_id=reply_message_id)
                elif file_type == GIF:
                    bot.send_animation(update.message.chat_id, action['action_data'],
                                       reply_message_id=reply_message_id)
                elif file_type == STICKER:
                    bot.send_sticker(update.message.chat_id, action['action_data'],
                                     reply_message_id=reply_message_id)
                elif file_type == AUDIO:
                    bot.send_audio(update.message.chat_id, action['action_data'],
                                   reply_message_id=reply_message_id)
                elif file_type == VOICE_MESSAGE:
                    bot.send_voice(update.message.chat_id, action['action_data'],
                                   reply_message_id=reply_message_id)
                elif file_type == DOCUMENT:
                    bot.send_document(update.message.chat_id, action['action_data'],
                                      reply_message_id=reply_message_id)

            except Exception as e:
                print(e)
                capture_exception(e)


def add_custom_handler(bot, update, args):
    log_to_db(bot, update)

    command_to_be_added = args[0]
    if not re.match(r'^[\da-z_]{1,32}$', command_to_be_added):
        bot.send_message(chat_id=update.message.chat_id, reply_to_message_id=update.message.message_id,
                         text="Sorry! Not a valid command.\n\nPlease refer to https://core.telegram.org/bots#commands "
                              "for command name limitations")

    else:
        media_type = None
        for i in MEDIA_TYPES:
            if i == IMAGE:
                if len(update.message.reply_to_message[i]) != 0:
                    media_type = i
                    break
            if i != IMAGE:
                if update.message.reply_to_message[i] is not None:
                    media_type = i
                    break

        if media_type is None:
            bot.send_message(chat_id=update.message.chat_id, reply_to_message_id=update.message.message_id,
                             text="Please reply to a gif or image to add as a command")
        else:
            try:
                if media_type == GIF:
                    file_id = update.message.reply_to_message.animation.file_id
                elif media_type == IMAGE:
                    file_id = update.message.reply_to_message.photo[-1].file_id
                elif media_type == VIDEO:
                    file_id = update.message.reply_to_message.video.file_id
                elif media_type == STICKER:
                    file_id = update.message.reply_to_message.sticker.file_id
                elif media_type == AUDIO:
                    file_id = update.message.reply_to_message.audio.file_id
                elif media_type == VOICE_MESSAGE:
                    file_id = update.message.reply_to_message.voice.file_id
                else:
                    file_id = update.message.reply_to_message.document.file_id

                already_in_use = bot.db.get_collection('commands').find_one({
                    "cmd": command_to_be_added,
                    "bot_id": bot.id
                })

                if already_in_use is not None:
                    bot.send_message(chat_id=update.message.chat_id, text="Sorry! This command is already in use")

                else:
                    command_entry = bot.db.get_collection('commands').insert_one({
                        "cmd": command_to_be_added,
                        "bot_id": bot.id,
                        "action": SEND_FILE,
                        "action_data": file_id,
                        "file_type": media_type
                    })

                    if command_entry.inserted_id:
                        bot.send_message(chat_id=update.message.chat_id,
                                         text="Saved! Now you can call /{}".format(command_to_be_added))

            except Exception as e:
                print(e)
                log_to_sentry(bot, update,
                              "There was an error saving custom command, command: {}".format(command_to_be_added))
                capture_exception(e)


def print_info(bot, update):
    if update.message.reply_to_message:
        print(update.message.reply_to_message)
    else:
        print(update)


def list_all_commands(bot, update):
    command_list = list(bot.db.get_collection('commands').find({"bot_id": bot.id}, {"cmd": 1}))
    message = "I support the following custom commands\n"
    for command in command_list:
        message += "/{}\n".format(command['cmd'])

    bot.send_message(chat_id=update.message.chat_id, text=message)


def alternating_caps(bot, update, args):
    if update.message.reply_to_message is not None:
        message_text = update.message.reply_to_message.text
    else:
        message_text = ' '.join(args)

    if message_text:
        altcap_text = ''
        for letter in message_text:
            if random() < 0.5:
                altcap_text += letter.lower()
            else:
                altcap_text += letter.upper()

        bot.send_message(chat_id=update.message.chat_id, text=altcap_text,
                         reply_to_message_id=update.message.message_id)
