from datetime import datetime
import os

sentry_id = os.environ.get('sentry_channel', 111)


def log_to_db(bot, update):
    message = update.message
    chat_id = message.chat.id
    user_id = message.from_user.id
    chat_type = message.chat.type
    command_used = message.text

    debug = os.environ.get('DEBUG', "True")
    if debug != "True":
        try:
            bot.cursor.execute("INSERT INTO doggobot_stats VALUES (%s, %s, %s, %s, %s)",
                               (chat_id, user_id, chat_type, command_used, datetime.now()))
            bot.conn.commit()
        except:
            print('Something went wrong while logging!' + str(update))


def log_to_sentry(bot, update, text):
    bot.send_message(chat_id=sentry_id, text=text)
