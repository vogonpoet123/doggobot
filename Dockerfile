FROM python:3.8.15-slim-buster

WORKDIR /bot
ADD . .
RUN ls /bot

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "bot.py"]