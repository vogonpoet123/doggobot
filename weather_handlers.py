import urllib
import requests
from logger import log_to_db
import os


def get_weather(bot, update, args):
    weather_key = os.environ.get('weather_key', None)
    request_url = "http://api.openweathermap.org/data/2.5/weather?q={}&APPID={}"

    if not args:
        update.message.reply_text('Please give me a valid location')

    else:
        city_name = urllib.parse.quote_plus(" ".join(args))
        weather = requests.get(request_url.format(
            city_name, weather_key)).json()
        weather_city = weather['name'] + ', ' + weather['sys']['country']
        weather_temp = weather['main']['temp'] - 273.15
        weather_cond = weather['weather'][0]['main']
        humidity = weather['main']['humidity']
        update.message.reply_text(
            "{}\nTemperature : {}°C\nWeather: {}\nHumidity: {}%".format(weather_city, round(weather_temp, 2),
                                                                        weather_cond, humidity))

    log_to_db(bot, update)
